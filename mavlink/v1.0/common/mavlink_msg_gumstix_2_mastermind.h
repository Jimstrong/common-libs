// MESSAGE gumstix_2_mastermind PACKING

#define MAVLINK_MSG_ID_gumstix_2_mastermind 151

typedef struct __mavlink_gumstix_2_mastermind_t
{
 int64_t time_usec; ///< Gumstix onboard time in usec 
 int32_t lon; ///< Gumstix GPS Longitude in degrees * 1E7
 int32_t lat; ///< Gumstix GPS Latitude in degrees * 1E7
 int32_t alt; ///< Gumstix GPS Altitude in degrees * 1E7
 float velocity_ned[3]; ///< Gumstix ned velocity in m/s
 float eulerAngle[3]; ///< Gumstix Euler Angle (roll, pitch, yaw) in degrees
 float angularRate[3]; ///< Gumstix Angular Rate (roll, pitch, yaw) in degrees/s
 float reserved_float[6]; ///< Reserved float array field for future usuage
 uint8_t reserved_bool[6]; ///< Reserved bool array field for future usuage
} mavlink_gumstix_2_mastermind_t;

#define MAVLINK_MSG_ID_gumstix_2_mastermind_LEN 86
#define MAVLINK_MSG_ID_151_LEN 86

#define MAVLINK_MSG_ID_gumstix_2_mastermind_CRC 185
#define MAVLINK_MSG_ID_151_CRC 185

#define MAVLINK_MSG_gumstix_2_mastermind_FIELD_VELOCITY_NED_LEN 3
#define MAVLINK_MSG_gumstix_2_mastermind_FIELD_EULERANGLE_LEN 3
#define MAVLINK_MSG_gumstix_2_mastermind_FIELD_ANGULARRATE_LEN 3
#define MAVLINK_MSG_gumstix_2_mastermind_FIELD_RESERVED_FLOAT_LEN 6
#define MAVLINK_MSG_gumstix_2_mastermind_FIELD_RESERVED_BOOL_LEN 6

#define MAVLINK_MESSAGE_INFO_gumstix_2_mastermind { \
	"gumstix_2_mastermind", \
	9, \
	{  { "time_usec", NULL, MAVLINK_TYPE_INT64_T, 0, 0, offsetof(mavlink_gumstix_2_mastermind_t, time_usec) }, \
         { "lon", NULL, MAVLINK_TYPE_INT32_T, 0, 8, offsetof(mavlink_gumstix_2_mastermind_t, lon) }, \
         { "lat", NULL, MAVLINK_TYPE_INT32_T, 0, 12, offsetof(mavlink_gumstix_2_mastermind_t, lat) }, \
         { "alt", NULL, MAVLINK_TYPE_INT32_T, 0, 16, offsetof(mavlink_gumstix_2_mastermind_t, alt) }, \
         { "velocity_ned", NULL, MAVLINK_TYPE_FLOAT, 3, 20, offsetof(mavlink_gumstix_2_mastermind_t, velocity_ned) }, \
         { "eulerAngle", NULL, MAVLINK_TYPE_FLOAT, 3, 32, offsetof(mavlink_gumstix_2_mastermind_t, eulerAngle) }, \
         { "angularRate", NULL, MAVLINK_TYPE_FLOAT, 3, 44, offsetof(mavlink_gumstix_2_mastermind_t, angularRate) }, \
         { "reserved_float", NULL, MAVLINK_TYPE_FLOAT, 6, 56, offsetof(mavlink_gumstix_2_mastermind_t, reserved_float) }, \
         { "reserved_bool", NULL, MAVLINK_TYPE_UINT8_T, 6, 80, offsetof(mavlink_gumstix_2_mastermind_t, reserved_bool) }, \
         } \
}


/**
 * @brief Pack a gumstix_2_mastermind message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time_usec Gumstix onboard time in usec 
 * @param lon Gumstix GPS Longitude in degrees * 1E7
 * @param lat Gumstix GPS Latitude in degrees * 1E7
 * @param alt Gumstix GPS Altitude in degrees * 1E7
 * @param velocity_ned Gumstix ned velocity in m/s
 * @param eulerAngle Gumstix Euler Angle (roll, pitch, yaw) in degrees
 * @param angularRate Gumstix Angular Rate (roll, pitch, yaw) in degrees/s
 * @param reserved_bool Reserved bool array field for future usuage
 * @param reserved_float Reserved float array field for future usuage
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_gumstix_2_mastermind_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
						       int64_t time_usec, int32_t lon, int32_t lat, int32_t alt, const float *velocity_ned, const float *eulerAngle, const float *angularRate, const uint8_t *reserved_bool, const float *reserved_float)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_gumstix_2_mastermind_LEN];
	_mav_put_int64_t(buf, 0, time_usec);
	_mav_put_int32_t(buf, 8, lon);
	_mav_put_int32_t(buf, 12, lat);
	_mav_put_int32_t(buf, 16, alt);
	_mav_put_float_array(buf, 20, velocity_ned, 3);
	_mav_put_float_array(buf, 32, eulerAngle, 3);
	_mav_put_float_array(buf, 44, angularRate, 3);
	_mav_put_float_array(buf, 56, reserved_float, 6);
	_mav_put_uint8_t_array(buf, 80, reserved_bool, 6);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_gumstix_2_mastermind_LEN);
#else
	mavlink_gumstix_2_mastermind_t packet;
	packet.time_usec = time_usec;
	packet.lon = lon;
	packet.lat = lat;
	packet.alt = alt;
	mav_array_memcpy(packet.velocity_ned, velocity_ned, sizeof(float)*3);
	mav_array_memcpy(packet.eulerAngle, eulerAngle, sizeof(float)*3);
	mav_array_memcpy(packet.angularRate, angularRate, sizeof(float)*3);
	mav_array_memcpy(packet.reserved_float, reserved_float, sizeof(float)*6);
	mav_array_memcpy(packet.reserved_bool, reserved_bool, sizeof(uint8_t)*6);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_gumstix_2_mastermind_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_gumstix_2_mastermind;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_gumstix_2_mastermind_LEN, MAVLINK_MSG_ID_gumstix_2_mastermind_CRC);
#else
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_gumstix_2_mastermind_LEN);
#endif
}

/**
 * @brief Pack a gumstix_2_mastermind message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time_usec Gumstix onboard time in usec 
 * @param lon Gumstix GPS Longitude in degrees * 1E7
 * @param lat Gumstix GPS Latitude in degrees * 1E7
 * @param alt Gumstix GPS Altitude in degrees * 1E7
 * @param velocity_ned Gumstix ned velocity in m/s
 * @param eulerAngle Gumstix Euler Angle (roll, pitch, yaw) in degrees
 * @param angularRate Gumstix Angular Rate (roll, pitch, yaw) in degrees/s
 * @param reserved_bool Reserved bool array field for future usuage
 * @param reserved_float Reserved float array field for future usuage
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_gumstix_2_mastermind_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
							   mavlink_message_t* msg,
						           int64_t time_usec,int32_t lon,int32_t lat,int32_t alt,const float *velocity_ned,const float *eulerAngle,const float *angularRate,const uint8_t *reserved_bool,const float *reserved_float)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_gumstix_2_mastermind_LEN];
	_mav_put_int64_t(buf, 0, time_usec);
	_mav_put_int32_t(buf, 8, lon);
	_mav_put_int32_t(buf, 12, lat);
	_mav_put_int32_t(buf, 16, alt);
	_mav_put_float_array(buf, 20, velocity_ned, 3);
	_mav_put_float_array(buf, 32, eulerAngle, 3);
	_mav_put_float_array(buf, 44, angularRate, 3);
	_mav_put_float_array(buf, 56, reserved_float, 6);
	_mav_put_uint8_t_array(buf, 80, reserved_bool, 6);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_gumstix_2_mastermind_LEN);
#else
	mavlink_gumstix_2_mastermind_t packet;
	packet.time_usec = time_usec;
	packet.lon = lon;
	packet.lat = lat;
	packet.alt = alt;
	mav_array_memcpy(packet.velocity_ned, velocity_ned, sizeof(float)*3);
	mav_array_memcpy(packet.eulerAngle, eulerAngle, sizeof(float)*3);
	mav_array_memcpy(packet.angularRate, angularRate, sizeof(float)*3);
	mav_array_memcpy(packet.reserved_float, reserved_float, sizeof(float)*6);
	mav_array_memcpy(packet.reserved_bool, reserved_bool, sizeof(uint8_t)*6);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_gumstix_2_mastermind_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_gumstix_2_mastermind;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_gumstix_2_mastermind_LEN, MAVLINK_MSG_ID_gumstix_2_mastermind_CRC);
#else
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_gumstix_2_mastermind_LEN);
#endif
}

/**
 * @brief Encode a gumstix_2_mastermind struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param gumstix_2_mastermind C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_gumstix_2_mastermind_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_gumstix_2_mastermind_t* gumstix_2_mastermind)
{
	return mavlink_msg_gumstix_2_mastermind_pack(system_id, component_id, msg, gumstix_2_mastermind->time_usec, gumstix_2_mastermind->lon, gumstix_2_mastermind->lat, gumstix_2_mastermind->alt, gumstix_2_mastermind->velocity_ned, gumstix_2_mastermind->eulerAngle, gumstix_2_mastermind->angularRate, gumstix_2_mastermind->reserved_bool, gumstix_2_mastermind->reserved_float);
}

/**
 * @brief Encode a gumstix_2_mastermind struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param gumstix_2_mastermind C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_gumstix_2_mastermind_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_gumstix_2_mastermind_t* gumstix_2_mastermind)
{
	return mavlink_msg_gumstix_2_mastermind_pack_chan(system_id, component_id, chan, msg, gumstix_2_mastermind->time_usec, gumstix_2_mastermind->lon, gumstix_2_mastermind->lat, gumstix_2_mastermind->alt, gumstix_2_mastermind->velocity_ned, gumstix_2_mastermind->eulerAngle, gumstix_2_mastermind->angularRate, gumstix_2_mastermind->reserved_bool, gumstix_2_mastermind->reserved_float);
}

/**
 * @brief Send a gumstix_2_mastermind message
 * @param chan MAVLink channel to send the message
 *
 * @param time_usec Gumstix onboard time in usec 
 * @param lon Gumstix GPS Longitude in degrees * 1E7
 * @param lat Gumstix GPS Latitude in degrees * 1E7
 * @param alt Gumstix GPS Altitude in degrees * 1E7
 * @param velocity_ned Gumstix ned velocity in m/s
 * @param eulerAngle Gumstix Euler Angle (roll, pitch, yaw) in degrees
 * @param angularRate Gumstix Angular Rate (roll, pitch, yaw) in degrees/s
 * @param reserved_bool Reserved bool array field for future usuage
 * @param reserved_float Reserved float array field for future usuage
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_gumstix_2_mastermind_send(mavlink_channel_t chan, int64_t time_usec, int32_t lon, int32_t lat, int32_t alt, const float *velocity_ned, const float *eulerAngle, const float *angularRate, const uint8_t *reserved_bool, const float *reserved_float)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_gumstix_2_mastermind_LEN];
	_mav_put_int64_t(buf, 0, time_usec);
	_mav_put_int32_t(buf, 8, lon);
	_mav_put_int32_t(buf, 12, lat);
	_mav_put_int32_t(buf, 16, alt);
	_mav_put_float_array(buf, 20, velocity_ned, 3);
	_mav_put_float_array(buf, 32, eulerAngle, 3);
	_mav_put_float_array(buf, 44, angularRate, 3);
	_mav_put_float_array(buf, 56, reserved_float, 6);
	_mav_put_uint8_t_array(buf, 80, reserved_bool, 6);
#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_gumstix_2_mastermind, buf, MAVLINK_MSG_ID_gumstix_2_mastermind_LEN, MAVLINK_MSG_ID_gumstix_2_mastermind_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_gumstix_2_mastermind, buf, MAVLINK_MSG_ID_gumstix_2_mastermind_LEN);
#endif
#else
	mavlink_gumstix_2_mastermind_t packet;
	packet.time_usec = time_usec;
	packet.lon = lon;
	packet.lat = lat;
	packet.alt = alt;
	mav_array_memcpy(packet.velocity_ned, velocity_ned, sizeof(float)*3);
	mav_array_memcpy(packet.eulerAngle, eulerAngle, sizeof(float)*3);
	mav_array_memcpy(packet.angularRate, angularRate, sizeof(float)*3);
	mav_array_memcpy(packet.reserved_float, reserved_float, sizeof(float)*6);
	mav_array_memcpy(packet.reserved_bool, reserved_bool, sizeof(uint8_t)*6);
#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_gumstix_2_mastermind, (const char *)&packet, MAVLINK_MSG_ID_gumstix_2_mastermind_LEN, MAVLINK_MSG_ID_gumstix_2_mastermind_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_gumstix_2_mastermind, (const char *)&packet, MAVLINK_MSG_ID_gumstix_2_mastermind_LEN);
#endif
#endif
}

#if MAVLINK_MSG_ID_gumstix_2_mastermind_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_gumstix_2_mastermind_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  int64_t time_usec, int32_t lon, int32_t lat, int32_t alt, const float *velocity_ned, const float *eulerAngle, const float *angularRate, const uint8_t *reserved_bool, const float *reserved_float)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char *buf = (char *)msgbuf;
	_mav_put_int64_t(buf, 0, time_usec);
	_mav_put_int32_t(buf, 8, lon);
	_mav_put_int32_t(buf, 12, lat);
	_mav_put_int32_t(buf, 16, alt);
	_mav_put_float_array(buf, 20, velocity_ned, 3);
	_mav_put_float_array(buf, 32, eulerAngle, 3);
	_mav_put_float_array(buf, 44, angularRate, 3);
	_mav_put_float_array(buf, 56, reserved_float, 6);
	_mav_put_uint8_t_array(buf, 80, reserved_bool, 6);
#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_gumstix_2_mastermind, buf, MAVLINK_MSG_ID_gumstix_2_mastermind_LEN, MAVLINK_MSG_ID_gumstix_2_mastermind_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_gumstix_2_mastermind, buf, MAVLINK_MSG_ID_gumstix_2_mastermind_LEN);
#endif
#else
	mavlink_gumstix_2_mastermind_t *packet = (mavlink_gumstix_2_mastermind_t *)msgbuf;
	packet->time_usec = time_usec;
	packet->lon = lon;
	packet->lat = lat;
	packet->alt = alt;
	mav_array_memcpy(packet->velocity_ned, velocity_ned, sizeof(float)*3);
	mav_array_memcpy(packet->eulerAngle, eulerAngle, sizeof(float)*3);
	mav_array_memcpy(packet->angularRate, angularRate, sizeof(float)*3);
	mav_array_memcpy(packet->reserved_float, reserved_float, sizeof(float)*6);
	mav_array_memcpy(packet->reserved_bool, reserved_bool, sizeof(uint8_t)*6);
#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_gumstix_2_mastermind, (const char *)packet, MAVLINK_MSG_ID_gumstix_2_mastermind_LEN, MAVLINK_MSG_ID_gumstix_2_mastermind_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_gumstix_2_mastermind, (const char *)packet, MAVLINK_MSG_ID_gumstix_2_mastermind_LEN);
#endif
#endif
}
#endif

#endif

// MESSAGE gumstix_2_mastermind UNPACKING


/**
 * @brief Get field time_usec from gumstix_2_mastermind message
 *
 * @return Gumstix onboard time in usec 
 */
static inline int64_t mavlink_msg_gumstix_2_mastermind_get_time_usec(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int64_t(msg,  0);
}

/**
 * @brief Get field lon from gumstix_2_mastermind message
 *
 * @return Gumstix GPS Longitude in degrees * 1E7
 */
static inline int32_t mavlink_msg_gumstix_2_mastermind_get_lon(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int32_t(msg,  8);
}

/**
 * @brief Get field lat from gumstix_2_mastermind message
 *
 * @return Gumstix GPS Latitude in degrees * 1E7
 */
static inline int32_t mavlink_msg_gumstix_2_mastermind_get_lat(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int32_t(msg,  12);
}

/**
 * @brief Get field alt from gumstix_2_mastermind message
 *
 * @return Gumstix GPS Altitude in degrees * 1E7
 */
static inline int32_t mavlink_msg_gumstix_2_mastermind_get_alt(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int32_t(msg,  16);
}

/**
 * @brief Get field velocity_ned from gumstix_2_mastermind message
 *
 * @return Gumstix ned velocity in m/s
 */
static inline uint16_t mavlink_msg_gumstix_2_mastermind_get_velocity_ned(const mavlink_message_t* msg, float *velocity_ned)
{
	return _MAV_RETURN_float_array(msg, velocity_ned, 3,  20);
}

/**
 * @brief Get field eulerAngle from gumstix_2_mastermind message
 *
 * @return Gumstix Euler Angle (roll, pitch, yaw) in degrees
 */
static inline uint16_t mavlink_msg_gumstix_2_mastermind_get_eulerAngle(const mavlink_message_t* msg, float *eulerAngle)
{
	return _MAV_RETURN_float_array(msg, eulerAngle, 3,  32);
}

/**
 * @brief Get field angularRate from gumstix_2_mastermind message
 *
 * @return Gumstix Angular Rate (roll, pitch, yaw) in degrees/s
 */
static inline uint16_t mavlink_msg_gumstix_2_mastermind_get_angularRate(const mavlink_message_t* msg, float *angularRate)
{
	return _MAV_RETURN_float_array(msg, angularRate, 3,  44);
}

/**
 * @brief Get field reserved_bool from gumstix_2_mastermind message
 *
 * @return Reserved bool array field for future usuage
 */
static inline uint16_t mavlink_msg_gumstix_2_mastermind_get_reserved_bool(const mavlink_message_t* msg, uint8_t *reserved_bool)
{
	return _MAV_RETURN_uint8_t_array(msg, reserved_bool, 6,  80);
}

/**
 * @brief Get field reserved_float from gumstix_2_mastermind message
 *
 * @return Reserved float array field for future usuage
 */
static inline uint16_t mavlink_msg_gumstix_2_mastermind_get_reserved_float(const mavlink_message_t* msg, float *reserved_float)
{
	return _MAV_RETURN_float_array(msg, reserved_float, 6,  56);
}

/**
 * @brief Decode a gumstix_2_mastermind message into a struct
 *
 * @param msg The message to decode
 * @param gumstix_2_mastermind C-struct to decode the message contents into
 */
static inline void mavlink_msg_gumstix_2_mastermind_decode(const mavlink_message_t* msg, mavlink_gumstix_2_mastermind_t* gumstix_2_mastermind)
{
#if MAVLINK_NEED_BYTE_SWAP
	gumstix_2_mastermind->time_usec = mavlink_msg_gumstix_2_mastermind_get_time_usec(msg);
	gumstix_2_mastermind->lon = mavlink_msg_gumstix_2_mastermind_get_lon(msg);
	gumstix_2_mastermind->lat = mavlink_msg_gumstix_2_mastermind_get_lat(msg);
	gumstix_2_mastermind->alt = mavlink_msg_gumstix_2_mastermind_get_alt(msg);
	mavlink_msg_gumstix_2_mastermind_get_velocity_ned(msg, gumstix_2_mastermind->velocity_ned);
	mavlink_msg_gumstix_2_mastermind_get_eulerAngle(msg, gumstix_2_mastermind->eulerAngle);
	mavlink_msg_gumstix_2_mastermind_get_angularRate(msg, gumstix_2_mastermind->angularRate);
	mavlink_msg_gumstix_2_mastermind_get_reserved_float(msg, gumstix_2_mastermind->reserved_float);
	mavlink_msg_gumstix_2_mastermind_get_reserved_bool(msg, gumstix_2_mastermind->reserved_bool);
#else
	memcpy(gumstix_2_mastermind, _MAV_PAYLOAD(msg), MAVLINK_MSG_ID_gumstix_2_mastermind_LEN);
#endif
}

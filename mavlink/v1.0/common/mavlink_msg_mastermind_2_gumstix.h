// MESSAGE mastermind_2_gumstix PACKING

#define MAVLINK_MSG_ID_mastermind_2_gumstix 152

typedef struct __mavlink_mastermind_2_gumstix_t
{
 int64_t time_usec; ///< mastermind onboard time in usec 
 float position[3]; ///< mastermind vision estimated position
 float velocity[3]; ///< mastermind vision estimated velocity
 float eulerAngle[3]; ///< mastermind vision estimated euler angles in degrees
 float cmd_position[4]; ///< command signals (x, y, z, heading) to gumstix from mastermind 
 float reserved_float[8]; ///< Reserved float array field for future usuage
 uint8_t reserved_bool[6]; ///< Reserved bool array field for future usuage
} mavlink_mastermind_2_gumstix_t;

#define MAVLINK_MSG_ID_mastermind_2_gumstix_LEN 98
#define MAVLINK_MSG_ID_152_LEN 98

#define MAVLINK_MSG_ID_mastermind_2_gumstix_CRC 83
#define MAVLINK_MSG_ID_152_CRC 83

#define MAVLINK_MSG_mastermind_2_gumstix_FIELD_POSITION_LEN 3
#define MAVLINK_MSG_mastermind_2_gumstix_FIELD_VELOCITY_LEN 3
#define MAVLINK_MSG_mastermind_2_gumstix_FIELD_EULERANGLE_LEN 3
#define MAVLINK_MSG_mastermind_2_gumstix_FIELD_CMD_POSITION_LEN 4
#define MAVLINK_MSG_mastermind_2_gumstix_FIELD_RESERVED_FLOAT_LEN 8
#define MAVLINK_MSG_mastermind_2_gumstix_FIELD_RESERVED_BOOL_LEN 6

#define MAVLINK_MESSAGE_INFO_mastermind_2_gumstix { \
	"mastermind_2_gumstix", \
	7, \
	{  { "time_usec", NULL, MAVLINK_TYPE_INT64_T, 0, 0, offsetof(mavlink_mastermind_2_gumstix_t, time_usec) }, \
         { "position", NULL, MAVLINK_TYPE_FLOAT, 3, 8, offsetof(mavlink_mastermind_2_gumstix_t, position) }, \
         { "velocity", NULL, MAVLINK_TYPE_FLOAT, 3, 20, offsetof(mavlink_mastermind_2_gumstix_t, velocity) }, \
         { "eulerAngle", NULL, MAVLINK_TYPE_FLOAT, 3, 32, offsetof(mavlink_mastermind_2_gumstix_t, eulerAngle) }, \
         { "cmd_position", NULL, MAVLINK_TYPE_FLOAT, 4, 44, offsetof(mavlink_mastermind_2_gumstix_t, cmd_position) }, \
         { "reserved_float", NULL, MAVLINK_TYPE_FLOAT, 8, 60, offsetof(mavlink_mastermind_2_gumstix_t, reserved_float) }, \
         { "reserved_bool", NULL, MAVLINK_TYPE_UINT8_T, 6, 92, offsetof(mavlink_mastermind_2_gumstix_t, reserved_bool) }, \
         } \
}


/**
 * @brief Pack a mastermind_2_gumstix message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time_usec mastermind onboard time in usec 
 * @param position mastermind vision estimated position
 * @param velocity mastermind vision estimated velocity
 * @param eulerAngle mastermind vision estimated euler angles in degrees
 * @param cmd_position command signals (x, y, z, heading) to gumstix from mastermind 
 * @param reserved_bool Reserved bool array field for future usuage
 * @param reserved_float Reserved float array field for future usuage
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_mastermind_2_gumstix_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
						       int64_t time_usec, const float *position, const float *velocity, const float *eulerAngle, const float *cmd_position, const uint8_t *reserved_bool, const float *reserved_float)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_mastermind_2_gumstix_LEN];
	_mav_put_int64_t(buf, 0, time_usec);
	_mav_put_float_array(buf, 8, position, 3);
	_mav_put_float_array(buf, 20, velocity, 3);
	_mav_put_float_array(buf, 32, eulerAngle, 3);
	_mav_put_float_array(buf, 44, cmd_position, 4);
	_mav_put_float_array(buf, 60, reserved_float, 8);
	_mav_put_uint8_t_array(buf, 92, reserved_bool, 6);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_mastermind_2_gumstix_LEN);
#else
	mavlink_mastermind_2_gumstix_t packet;
	packet.time_usec = time_usec;
	mav_array_memcpy(packet.position, position, sizeof(float)*3);
	mav_array_memcpy(packet.velocity, velocity, sizeof(float)*3);
	mav_array_memcpy(packet.eulerAngle, eulerAngle, sizeof(float)*3);
	mav_array_memcpy(packet.cmd_position, cmd_position, sizeof(float)*4);
	mav_array_memcpy(packet.reserved_float, reserved_float, sizeof(float)*8);
	mav_array_memcpy(packet.reserved_bool, reserved_bool, sizeof(uint8_t)*6);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_mastermind_2_gumstix_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_mastermind_2_gumstix;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_mastermind_2_gumstix_LEN, MAVLINK_MSG_ID_mastermind_2_gumstix_CRC);
#else
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_mastermind_2_gumstix_LEN);
#endif
}

/**
 * @brief Pack a mastermind_2_gumstix message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time_usec mastermind onboard time in usec 
 * @param position mastermind vision estimated position
 * @param velocity mastermind vision estimated velocity
 * @param eulerAngle mastermind vision estimated euler angles in degrees
 * @param cmd_position command signals (x, y, z, heading) to gumstix from mastermind 
 * @param reserved_bool Reserved bool array field for future usuage
 * @param reserved_float Reserved float array field for future usuage
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_mastermind_2_gumstix_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
							   mavlink_message_t* msg,
						           int64_t time_usec,const float *position,const float *velocity,const float *eulerAngle,const float *cmd_position,const uint8_t *reserved_bool,const float *reserved_float)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_mastermind_2_gumstix_LEN];
	_mav_put_int64_t(buf, 0, time_usec);
	_mav_put_float_array(buf, 8, position, 3);
	_mav_put_float_array(buf, 20, velocity, 3);
	_mav_put_float_array(buf, 32, eulerAngle, 3);
	_mav_put_float_array(buf, 44, cmd_position, 4);
	_mav_put_float_array(buf, 60, reserved_float, 8);
	_mav_put_uint8_t_array(buf, 92, reserved_bool, 6);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_mastermind_2_gumstix_LEN);
#else
	mavlink_mastermind_2_gumstix_t packet;
	packet.time_usec = time_usec;
	mav_array_memcpy(packet.position, position, sizeof(float)*3);
	mav_array_memcpy(packet.velocity, velocity, sizeof(float)*3);
	mav_array_memcpy(packet.eulerAngle, eulerAngle, sizeof(float)*3);
	mav_array_memcpy(packet.cmd_position, cmd_position, sizeof(float)*4);
	mav_array_memcpy(packet.reserved_float, reserved_float, sizeof(float)*8);
	mav_array_memcpy(packet.reserved_bool, reserved_bool, sizeof(uint8_t)*6);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_mastermind_2_gumstix_LEN);
#endif

	msg->msgid = MAVLINK_MSG_ID_mastermind_2_gumstix;
#if MAVLINK_CRC_EXTRA
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_mastermind_2_gumstix_LEN, MAVLINK_MSG_ID_mastermind_2_gumstix_CRC);
#else
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_mastermind_2_gumstix_LEN);
#endif
}

/**
 * @brief Encode a mastermind_2_gumstix struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param mastermind_2_gumstix C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_mastermind_2_gumstix_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_mastermind_2_gumstix_t* mastermind_2_gumstix)
{
	return mavlink_msg_mastermind_2_gumstix_pack(system_id, component_id, msg, mastermind_2_gumstix->time_usec, mastermind_2_gumstix->position, mastermind_2_gumstix->velocity, mastermind_2_gumstix->eulerAngle, mastermind_2_gumstix->cmd_position, mastermind_2_gumstix->reserved_bool, mastermind_2_gumstix->reserved_float);
}

/**
 * @brief Encode a mastermind_2_gumstix struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param mastermind_2_gumstix C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_mastermind_2_gumstix_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_mastermind_2_gumstix_t* mastermind_2_gumstix)
{
	return mavlink_msg_mastermind_2_gumstix_pack_chan(system_id, component_id, chan, msg, mastermind_2_gumstix->time_usec, mastermind_2_gumstix->position, mastermind_2_gumstix->velocity, mastermind_2_gumstix->eulerAngle, mastermind_2_gumstix->cmd_position, mastermind_2_gumstix->reserved_bool, mastermind_2_gumstix->reserved_float);
}

/**
 * @brief Send a mastermind_2_gumstix message
 * @param chan MAVLink channel to send the message
 *
 * @param time_usec mastermind onboard time in usec 
 * @param position mastermind vision estimated position
 * @param velocity mastermind vision estimated velocity
 * @param eulerAngle mastermind vision estimated euler angles in degrees
 * @param cmd_position command signals (x, y, z, heading) to gumstix from mastermind 
 * @param reserved_bool Reserved bool array field for future usuage
 * @param reserved_float Reserved float array field for future usuage
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_mastermind_2_gumstix_send(mavlink_channel_t chan, int64_t time_usec, const float *position, const float *velocity, const float *eulerAngle, const float *cmd_position, const uint8_t *reserved_bool, const float *reserved_float)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char buf[MAVLINK_MSG_ID_mastermind_2_gumstix_LEN];
	_mav_put_int64_t(buf, 0, time_usec);
	_mav_put_float_array(buf, 8, position, 3);
	_mav_put_float_array(buf, 20, velocity, 3);
	_mav_put_float_array(buf, 32, eulerAngle, 3);
	_mav_put_float_array(buf, 44, cmd_position, 4);
	_mav_put_float_array(buf, 60, reserved_float, 8);
	_mav_put_uint8_t_array(buf, 92, reserved_bool, 6);
#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_mastermind_2_gumstix, buf, MAVLINK_MSG_ID_mastermind_2_gumstix_LEN, MAVLINK_MSG_ID_mastermind_2_gumstix_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_mastermind_2_gumstix, buf, MAVLINK_MSG_ID_mastermind_2_gumstix_LEN);
#endif
#else
	mavlink_mastermind_2_gumstix_t packet;
	packet.time_usec = time_usec;
	mav_array_memcpy(packet.position, position, sizeof(float)*3);
	mav_array_memcpy(packet.velocity, velocity, sizeof(float)*3);
	mav_array_memcpy(packet.eulerAngle, eulerAngle, sizeof(float)*3);
	mav_array_memcpy(packet.cmd_position, cmd_position, sizeof(float)*4);
	mav_array_memcpy(packet.reserved_float, reserved_float, sizeof(float)*8);
	mav_array_memcpy(packet.reserved_bool, reserved_bool, sizeof(uint8_t)*6);
#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_mastermind_2_gumstix, (const char *)&packet, MAVLINK_MSG_ID_mastermind_2_gumstix_LEN, MAVLINK_MSG_ID_mastermind_2_gumstix_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_mastermind_2_gumstix, (const char *)&packet, MAVLINK_MSG_ID_mastermind_2_gumstix_LEN);
#endif
#endif
}

#if MAVLINK_MSG_ID_mastermind_2_gumstix_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_mastermind_2_gumstix_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  int64_t time_usec, const float *position, const float *velocity, const float *eulerAngle, const float *cmd_position, const uint8_t *reserved_bool, const float *reserved_float)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
	char *buf = (char *)msgbuf;
	_mav_put_int64_t(buf, 0, time_usec);
	_mav_put_float_array(buf, 8, position, 3);
	_mav_put_float_array(buf, 20, velocity, 3);
	_mav_put_float_array(buf, 32, eulerAngle, 3);
	_mav_put_float_array(buf, 44, cmd_position, 4);
	_mav_put_float_array(buf, 60, reserved_float, 8);
	_mav_put_uint8_t_array(buf, 92, reserved_bool, 6);
#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_mastermind_2_gumstix, buf, MAVLINK_MSG_ID_mastermind_2_gumstix_LEN, MAVLINK_MSG_ID_mastermind_2_gumstix_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_mastermind_2_gumstix, buf, MAVLINK_MSG_ID_mastermind_2_gumstix_LEN);
#endif
#else
	mavlink_mastermind_2_gumstix_t *packet = (mavlink_mastermind_2_gumstix_t *)msgbuf;
	packet->time_usec = time_usec;
	mav_array_memcpy(packet->position, position, sizeof(float)*3);
	mav_array_memcpy(packet->velocity, velocity, sizeof(float)*3);
	mav_array_memcpy(packet->eulerAngle, eulerAngle, sizeof(float)*3);
	mav_array_memcpy(packet->cmd_position, cmd_position, sizeof(float)*4);
	mav_array_memcpy(packet->reserved_float, reserved_float, sizeof(float)*8);
	mav_array_memcpy(packet->reserved_bool, reserved_bool, sizeof(uint8_t)*6);
#if MAVLINK_CRC_EXTRA
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_mastermind_2_gumstix, (const char *)packet, MAVLINK_MSG_ID_mastermind_2_gumstix_LEN, MAVLINK_MSG_ID_mastermind_2_gumstix_CRC);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_mastermind_2_gumstix, (const char *)packet, MAVLINK_MSG_ID_mastermind_2_gumstix_LEN);
#endif
#endif
}
#endif

#endif

// MESSAGE mastermind_2_gumstix UNPACKING


/**
 * @brief Get field time_usec from mastermind_2_gumstix message
 *
 * @return mastermind onboard time in usec 
 */
static inline int64_t mavlink_msg_mastermind_2_gumstix_get_time_usec(const mavlink_message_t* msg)
{
	return _MAV_RETURN_int64_t(msg,  0);
}

/**
 * @brief Get field position from mastermind_2_gumstix message
 *
 * @return mastermind vision estimated position
 */
static inline uint16_t mavlink_msg_mastermind_2_gumstix_get_position(const mavlink_message_t* msg, float *position)
{
	return _MAV_RETURN_float_array(msg, position, 3,  8);
}

/**
 * @brief Get field velocity from mastermind_2_gumstix message
 *
 * @return mastermind vision estimated velocity
 */
static inline uint16_t mavlink_msg_mastermind_2_gumstix_get_velocity(const mavlink_message_t* msg, float *velocity)
{
	return _MAV_RETURN_float_array(msg, velocity, 3,  20);
}

/**
 * @brief Get field eulerAngle from mastermind_2_gumstix message
 *
 * @return mastermind vision estimated euler angles in degrees
 */
static inline uint16_t mavlink_msg_mastermind_2_gumstix_get_eulerAngle(const mavlink_message_t* msg, float *eulerAngle)
{
	return _MAV_RETURN_float_array(msg, eulerAngle, 3,  32);
}

/**
 * @brief Get field cmd_position from mastermind_2_gumstix message
 *
 * @return command signals (x, y, z, heading) to gumstix from mastermind 
 */
static inline uint16_t mavlink_msg_mastermind_2_gumstix_get_cmd_position(const mavlink_message_t* msg, float *cmd_position)
{
	return _MAV_RETURN_float_array(msg, cmd_position, 4,  44);
}

/**
 * @brief Get field reserved_bool from mastermind_2_gumstix message
 *
 * @return Reserved bool array field for future usuage
 */
static inline uint16_t mavlink_msg_mastermind_2_gumstix_get_reserved_bool(const mavlink_message_t* msg, uint8_t *reserved_bool)
{
	return _MAV_RETURN_uint8_t_array(msg, reserved_bool, 6,  92);
}

/**
 * @brief Get field reserved_float from mastermind_2_gumstix message
 *
 * @return Reserved float array field for future usuage
 */
static inline uint16_t mavlink_msg_mastermind_2_gumstix_get_reserved_float(const mavlink_message_t* msg, float *reserved_float)
{
	return _MAV_RETURN_float_array(msg, reserved_float, 8,  60);
}

/**
 * @brief Decode a mastermind_2_gumstix message into a struct
 *
 * @param msg The message to decode
 * @param mastermind_2_gumstix C-struct to decode the message contents into
 */
static inline void mavlink_msg_mastermind_2_gumstix_decode(const mavlink_message_t* msg, mavlink_mastermind_2_gumstix_t* mastermind_2_gumstix)
{
#if MAVLINK_NEED_BYTE_SWAP
	mastermind_2_gumstix->time_usec = mavlink_msg_mastermind_2_gumstix_get_time_usec(msg);
	mavlink_msg_mastermind_2_gumstix_get_position(msg, mastermind_2_gumstix->position);
	mavlink_msg_mastermind_2_gumstix_get_velocity(msg, mastermind_2_gumstix->velocity);
	mavlink_msg_mastermind_2_gumstix_get_eulerAngle(msg, mastermind_2_gumstix->eulerAngle);
	mavlink_msg_mastermind_2_gumstix_get_cmd_position(msg, mastermind_2_gumstix->cmd_position);
	mavlink_msg_mastermind_2_gumstix_get_reserved_float(msg, mastermind_2_gumstix->reserved_float);
	mavlink_msg_mastermind_2_gumstix_get_reserved_bool(msg, mastermind_2_gumstix->reserved_bool);
#else
	memcpy(mastermind_2_gumstix, _MAV_PAYLOAD(msg), MAVLINK_MSG_ID_mastermind_2_gumstix_LEN);
#endif
}

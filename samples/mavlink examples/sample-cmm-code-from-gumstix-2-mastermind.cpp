/*
 * cam.cpp
 *
 *  Created on: Mar 15, 2011

 */

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>

#include "uav.h"
#include "cam.h"
#include "state.h"
#include "ctl.h"

extern EQUILIBRIUM _equ_Hover;
extern clsState _state;
extern clsCTL _ctl;
extern double Psimeasureall[4];
/*
 * clsCAM
 */


clsCAM::clsCAM()
{
	m_nsCAM = -1;

	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutex_init(&m_mtxCAM, &attr);
}

clsCAM::~clsCAM()
{
}

BOOL clsCAM::Init()
{
	m_nsCAM = open("/dev/serusb3", O_RDWR|O_NONBLOCK);

	if (m_nsCAM == -1) {
			printf("[CAM] open serusb3 failed!\n");
			return FALSE;
	}

	termios termCAM;
    tcgetattr(m_nsCAM, &termCAM);

	cfsetispeed(&termCAM, B115200);				//input and output baudrate
	cfsetospeed(&termCAM, B115200);

	termCAM.c_cflag &= ~(CS5|CS6|CS7|HUPCL|IHFLOW|OHFLOW|PARENB|CSTOPB|CSIZE); // Note: must do ~CSIZE before CS8
	termCAM.c_cflag |= (CS8 | CLOCAL | CREAD);
	termCAM.c_iflag = 0; // Raw mode for input
	termCAM.c_oflag = 0; // Raw mode for output

	tcflush(m_nsCAM, TCIOFLUSH);
	tcsetattr(m_nsCAM, TCSANOW, &termCAM);

	return TRUE;
}

BOOL clsCAM::InitThread()
{
	m_nBuffer = 0;
	m_tInfo0 = -1;

	m_nInfo=0;

	m_DetectState = 0;
	::memset(&m_targetInfo0, 0, sizeof(TARGETINFO));
	::memset(&m_targetState0, 0, sizeof(TARGETSTATE));
	::memset(&m_targetInfo, 0, sizeof(BJ_TARGET));
	::memset(&m_targetInfoUpdate, 0, sizeof(BJ_TARGET));
	::memset(&m_UavBJpose, 0, sizeof(UAVBJPOSE));

	int index;

	for (index = 0; index < MAX_CAMINFO; index++) {
		m_tInfo[index]  = 0;
		m_info[index].l = 0;
		m_info[index].m = 0;
		m_info[index].n = 0;
	}

	tStart = 0;

	m_releaseTargetFinalPhase = 0;
	m_bVisionFinalPhaseFlag = false;

	printf("[CAM] start\n");

	return TRUE;
}

void clsCAM::handle_message(mavlink_message_t *msg){
	switch (msg->msgid) {
			case MAVLINK_MSG_ID_mastermind_2_gumstix:{
				mavlink_mastermind_2_gumstix_t mastermind2gumsitxData;
				mavlink_msg_mastermind_2_gumstix_decode(msg, &mastermind2gumsitxData);

				//printf("[cam] %.2f %.2f\n", mastermind2gumsitxData.time_usec/1e6, mastermind2gumsitxData.position[1]);
				////			for gimble case;
				double abc[3] = {(-1.0)*_state.GetState().a, (-1.0)*_state.GetState().b, 0};
				double r_t_c[3] = {mastermind2gumsitxData.position[0], mastermind2gumsitxData.position[1], mastermind2gumsitxData.position[2]};

				double camera_body[3] = {0};
				B2G(abc, r_t_c, camera_body);
				camera_body[0] += 0.3;
				camera_body[1] += 0;
				camera_body[2] += 0.13;

				for(int i = 0 ; i < 6; i++) m_targetInfoUpdate.flags[i] = mastermind2gumsitxData.reserved_bool[i];

				double abc_ned[3] = {_state.GetState().a, _state.GetState().b, _state.GetState().c};
				double delta_body[3] = {camera_body[0], camera_body[1], camera_body[2]};
				double delta_ned[3] = {0};
				B2G(abc_ned, delta_body, delta_ned);

					// in ship frame
				m_targetInfoUpdate.tvec[0] = delta_ned[0]*cos(Psimeasureall[1]) + delta_ned[1]*sin(Psimeasureall[1]);
				m_targetInfoUpdate.tvec[1] = (-1)*delta_ned[0]*sin(Psimeasureall[1]) + delta_ned[1]*cos(Psimeasureall[1]);
				m_targetInfoUpdate.tvec[2] = delta_ned[2];

				if(_ctl.m_taskCounter % 2 == 0 && _ctl.m_taskCounter > 0){ // release the target phase
						if(_state.GetState().z < CONFUSE_VISION_HEIGHT){ //the height of the helicopter;
							double dc = _state.GetState().c - Psimeasureall[1]; INPI(dc);
							if ( ::fabs(dc) < PI/2.0 ){ // the helicopter has the same heading direction as the ship
									m_targetInfoUpdate.tvec[0] = m_targetInfoUpdate.tvec[0] - 1;
							}
							else{ // the helicopter has the opposite heading direction as the ship
									m_targetInfoUpdate.tvec[0] = m_targetInfoUpdate.tvec[0] + 1;
							}
						}
						else{
							if (m_targetInfoUpdate.flags[0] == 1){
									SetVisionFinalPhase();
							}
						}
					}
					break;
				}
			 }
}

int clsCAM::EveryRun()
{
		char buf[300];
		mavlink_message_t message;

		mavlink_gumstix_2_mastermind_t data2mastermind;
		memset(&data2mastermind, 0, sizeof(mavlink_gumstix_2_mastermind_t));

		data2mastermind.time_usec = ::GetTime()*1e6;
		mavlink_msg_gumstix_2_mastermind_encode(10, 200, &message, &data2mastermind);
		unsigned len = mavlink_msg_to_send_buffer((uint8_t*)buf, &message);

		int nWriteBytes = write(m_nsCAM, buf, len);

		// receive data;
		int nRead = read(m_nsCAM, m_buffer, MAX_CAMBUFFER);	//get data as many as it can
		if (nRead < 0)
		{
			return true;
		}

		mavlink_message_t msg;
		mavlink_status_t status;
		for (int i = 0; i < nRead; i++) {
			if (mavlink_parse_char(MAVLINK_COMM_0, m_buffer[i], &msg, &status)) {
					handle_message(&msg);
			}
		}
		return TRUE;
}

void clsCAM::ExitThread()
{
//	if (m_pfCAM != NULL) ::fclose(m_pfCAM);
	printf("[CAM] quit\n");
}

BJ_TARGET clsCAM::GetVisionTargetInfo() {  //return relative position in NED frame;
		return m_targetInfoUpdate;
}
/*
 * clsVision
 */
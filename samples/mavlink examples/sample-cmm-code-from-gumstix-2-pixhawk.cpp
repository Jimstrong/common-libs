//svo.cpp
//implementation file for servo related class & functions

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
using namespace std;

#include "matrix.h"

#include "uav.h"
#include "svo.h"
#include "state.h"
#include "ctl.h"


extern clsState _state;
extern EQUILIBRIUM _equ_Hover;
extern clsIM9	_im9;

BOOL clsSVO::InitThread()
{
	 // Setup serial port to communicate with PX4FMU
		m_nsSVO= ::open("/dev/ser2", O_RDWR | O_NOCTTY | O_NDELAY | O_NONBLOCK);
		if (m_nsSVO == -1)
		{
			printf("[svo] Could not open port! \n");
			return FALSE;
		}

	    termios term;
	    memset(&term, 0, sizeof(term));
	    tcgetattr(m_nsSVO, &term);
	    speed_t baudrate = 230400;
	    cfsetispeed(&term, baudrate);
	    cfsetospeed(&term, baudrate);

	    // Input flags - Turn off input processing
	    // convert break to null byte, no CR to NL translation,
	    // no NL to CR translation, don't mark parity errors or breaks
	    // no input parity check, don't strip high bit off,
	    // no XON/XOFF software flow control
	    term.c_iflag &= ~(IGNBRK | BRKINT | ICRNL | INLCR | PARMRK | INPCK | ISTRIP | IXON);

	    // Output flags - Turn off output processing
	    // no CR to NL translation, no NL to CR-NL translation,
	    // no NL to CR translation, no column 0 CR suppression,
	    // no Ctrl-D suppression, no fill characters, no case mapping,
	    // no local output processing
	    term.c_oflag &= ~(OCRNL | ONLCR | ONLRET | ONOCR | OFILL | OPOST);

	    // No line processing:
	    // echo off, echo newline off, canonical mode off,
	    // extended input processing off, signal chars off
	    term.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);

	    // Turn off character processing
	    // clear current char size mask, no parity checking,
	    // no output processing, force 8 bit input
	    term.c_cflag &= ~(CSIZE | PARENB);
	    term.c_cflag |= CS8;

	    // One input byte is enough to return from read()
	    // Inter-character timer off
	    term.c_cc[VMIN]  = 1;
	    term.c_cc[VTIME] = 10; // was 0

		tcsetattr(m_nsSVO, TCSANOW, &term);
		tcflush(m_nsSVO, TCIOFLUSH);

	    printf("[svo] Start\n");

	    return TRUE;
}

void clsSVO::PutCommand(COMMAND *pCmd)
{
	pthread_mutex_lock(&m_mtxCmd);
	m_cmd = *pCmd;
	pthread_mutex_unlock(&m_mtxCmd);
}

void clsSVO::GetCommand(COMMAND *pCmd)
{
	pthread_mutex_lock(&m_mtxCmd);

	if(m_cmd.code == 0)
		pCmd->code = 0;
	else
	{
		*pCmd = m_cmd;
		m_cmd.code = 0;
	}

	pthread_mutex_unlock(&m_mtxCmd);
}

void clsSVO::SetTrimvalue()
{
	m_svoEqu.aileron = 0.5*_equ_Hover.ea + 0.5*m_avgTrimvalue.aileron;
	m_svoEqu.elevator = 0.5*_equ_Hover.ee + 0.5*m_avgTrimvalue.elevator;
	m_svoEqu.auxiliary = 0.5*_equ_Hover.eu + 0.5*m_avgTrimvalue.auxiliary;
	m_svoEqu.rudder = 0.5*_equ_Hover.er + 0.5*m_avgTrimvalue.rudder;
	m_svoEqu.throttle = 0.5*_equ_Hover.et + 0.5*m_avgTrimvalue.throttle;

	m_svoSet = m_svoEqu;	// put the servo deflections to the new trimvalues

	m_tTrimvalue = ::GetTime();
}

BOOL clsSVO::ProcessCommand(COMMAND *pCmd)
{
	COMMAND &cmd = *pCmd;
//	char *paraCmd = cmd.parameter;
	BOOL bProcess = TRUE;

	switch(cmd.code)
	{
	case COMMAND_GETTRIM:
//		m_bTrimvalue = TRUE;
		/* set all variables concering get trimvalue to zeros */
		m_nTrimCount = 0;
		m_avgTrimvalue.aileron = _equ_Hover.ea;
		m_avgTrimvalue.auxiliary = _equ_Hover.eu;
		m_avgTrimvalue.elevator = _equ_Hover.er;
		m_avgTrimvalue.rudder = _equ_Hover.er;
		m_avgTrimvalue.throttle = _equ_Hover.et;
		break;

	case COMMAND_HOLD:
		m_bTrimvalue = TRUE;
		m_trimT0 = ::GetTime();
		break;

	default:
		bProcess = FALSE;
		break;
	}
	return bProcess;
}

BOOL clsSVO::ValidateTrimvalue()
{
	UAVSTATE &state = _state.GetState();
	BOOL bValid =
		::fabs(state.u) <= THRESHOLDHIGH_U &&
		::fabs(state.v) <= THRESHOLDHIGH_V &&
		::fabs(state.w) <= THRESHOLDHIGH_W &&
		::fabs(state.p) <= THRESHOLDHIGH_P &&
		::fabs(state.q) <= THRESHOLDHIGH_Q &&
		::fabs(state.r) <= THRESHOLDHIGH_R;

	return bValid;
}

int clsSVO::EveryRun()
{
	/// get autocontrol signal from innerloop
	HELICOPTERRUDDER sig;
	_state.GetSIG(&sig);

	char buf[300];
	mavlink_message_t message;

	mavlink_gumstix_2_pxhawk_t data2PxHawk;
	memset(&data2PxHawk, 0, sizeof(data2PxHawk));

	data2PxHawk.controlSignal[0] = sig.aileron;
	data2PxHawk.controlSignal[1] = sig.elevator;
	data2PxHawk.controlSignal[2] = sig.rudder;
	data2PxHawk.controlSignal[3] = sig.throttle;
	data2PxHawk.controlSignal[4] = sig.auxiliary;

	mavlink_msg_gumstix_2_pxhawk_encode(10, 200, &message, &data2PxHawk);
	unsigned len = mavlink_msg_to_send_buffer((uint8_t*)buf, &message);

	int nWriteBytes = write(m_nsSVO, buf, len);

	uint8_t cp[2048];
	int count = read(m_nsSVO, cp, 2048);

	mavlink_message_t msg;
	mavlink_status_t status;
	for (ssize_t i = 0; i < count; i++) {
			if (mavlink_parse_char(MAVLINK_COMM_1, cp[i], &msg, &status)) {
					/* handle generic messages and commands */
					handle_message(&msg);
		}
	}
	return TRUE;
}

void clsSVO::handle_message(mavlink_message_t *msg){
	switch (msg->msgid) {
	case MAVLINK_MSG_ID_SERVO_OUTPUT_RAW:
		mavlink_servo_output_raw_t raw_servo_output;
		mavlink_msg_servo_output_raw_decode(msg, &raw_servo_output);

			m_tSVO0 = GetTime();

			m_svo0.aileron = raw_servo_output.servo1_raw*0.01 - 1;
			m_svo0.elevator = raw_servo_output.servo2_raw*0.01 - 1;
			m_svo0.throttle = raw_servo_output.servo4_raw*0.01 - 1;
			m_svo0.rudder = raw_servo_output.servo3_raw*0.01 - 1;
			m_svo0.auxiliary = raw_servo_output.servo5_raw*0.01 - 1;

			//printf("[svo] %.2f %.2f\n", m_svo0.aileron, m_svo0.elevator);
			/// copy into memory servo time, raw data and translated data
			pthread_mutex_lock(&m_mtxSVO);
			if (m_nSVO != MAX_SVO) {
					m_tSVO[m_nSVO] = m_tSVO0;
					m_svo[m_nSVO++] = m_svo0;
			}
			pthread_mutex_unlock(&m_mtxSVO);
	break;
	/*
	case MAVLINK_MSG_ID_RC_CHANNELS:
				mavlink_rc_channels_t rc_channels_scaled;
				mavlink_msg_rc_channels_decode(msg, &rc_channels_scaled);

				m_tSVO0 = GetTime();
				// Update pack
				double values[6], channels[6];
				memset(values, 0, sizeof(double)*6);
				memset(channels, 0, sizeof(double)*6);
				values[0] = (double)rc_channels_scaled.chan1_raw;
				values[1] = (double)rc_channels_scaled.chan2_raw;
				values[2] = (double)rc_channels_scaled.chan6_raw;
				values[3] = (double)rc_channels_scaled.chan4_raw;
				values[4] = (double)rc_channels_scaled.chan3_raw;
				values[5] = (double)rc_channels_scaled.chan16_raw;

				double trim; trim = 1500;
				double dz; dz = 0;
				double max; max = 2000;
				double min; min = 1000;

				for (int i = 0; i < 6; i++){
					if (values[i] > (trim + dz)) {
							channels[i] = (values[i] - trim - dz) /(max - trim - dz);
					} else if (values[i] < (trim - dz)) {
							channels[i] = (values[i] - trim + dz) /(trim - min - dz);
					} else {
							channels[i] = 0.0f;
					}
				}

				m_svo0.aileron = channels[0];
				m_svo0.elevator = channels[1];
				m_svo0.throttle = channels[2];
				m_svo0.rudder = channels[3];
				m_svo0.auxiliary = channels[4];
				m_svo0.sv6 = channels[5]; //This is not the switch mode;

				/// copy into memory servo time, raw data and translated data
				pthread_mutex_lock(&m_mtxSVO);
				if (m_nSVO != MAX_SVO) {
						m_tSVO[m_nSVO] = m_tSVO0;
						m_svo[m_nSVO++] = m_svo0;
				}
				pthread_mutex_unlock(&m_mtxSVO);

				break;
		*/
		default:
			break;
	}
}

BOOL clsSVO::CalcNewTrim()
{
	if( ValidateTrimvalue() )
	{
		m_avgTrimvalue.aileron = SVO_WEIGHT1*m_svo0.aileron + (1-SVO_WEIGHT1)*m_avgTrimvalue.aileron;
		m_avgTrimvalue.elevator = SVO_WEIGHT1*m_svo0.elevator + (1-SVO_WEIGHT1)*m_avgTrimvalue.elevator;
		m_avgTrimvalue.auxiliary = SVO_WEIGHT1*m_svo0.auxiliary + (1-SVO_WEIGHT1)*m_avgTrimvalue.auxiliary;
		m_avgTrimvalue.rudder = SVO_WEIGHT1*m_svo0.rudder + (1-SVO_WEIGHT1)*m_avgTrimvalue.rudder;
		m_avgTrimvalue.throttle = SVO_WEIGHT1*m_svo0.throttle + (1-SVO_WEIGHT1)*m_avgTrimvalue.throttle;
		return TRUE;
	}
	return FALSE;
}

void clsSVO::ExitThread()
{
	::close(m_nsSVO);
	printf("[SVO] quit\n");
}

void clsSVO::SetRudder(HELICOPTERRUDDER *pRudder)
{
	HELICOPTERRUDDER rudder = *pRudder;

	rudder.aileron = range(rudder.aileron, -0.5, 0.5);
	rudder.elevator = range(rudder.elevator, -0.5, 0.5);
	rudder.auxiliary = range(rudder.auxiliary, -0.7, 0.3);
	rudder.rudder = range(rudder.rudder, -0.5, 0.5);
	rudder.throttle = range(rudder.throttle, 0.2, 0.8);

	char szSVOCommand[114]; int nPosition;
	nPosition = 15000+(int)(5000*rudder.aileron);
	sprintf(szSVOCommand, "SV3 M%d\r M%d\r", nPosition, nPosition);

	nPosition = 15000+(int)(5000*rudder.elevator);
	sprintf(szSVOCommand + 19, "SV4 M%d\r M%d\r", nPosition, nPosition);

	nPosition = 15000+(int)(5000*rudder.auxiliary);
	sprintf(szSVOCommand + 38, "SV5 M%d\r M%d\r", nPosition, nPosition);

	nPosition = 15000+(int)(5000*rudder.rudder);
	sprintf(szSVOCommand + 57, "SV1 M%d\r M%d\r", nPosition, nPosition);

	nPosition = 10000+(int)(10000*rudder.throttle);
	sprintf(szSVOCommand + 76, "SV0 M%d\r M%d\r", nPosition, nPosition);

/* added by LPD for 2013-UAVGP 2013-08-02
 * channel 3 for grasper control
 * svo = -0.0314 for grasp
 * svo = 0.9675 for release
 */
	if (_ctl.GetGrasperStatus()){ //true for release
		nPosition = 15000+(int)(5000*0.9675);
//		printf("release\n");
	}
	else{
		nPosition = 10000;
//		printf("Grasp\n");
	}
	sprintf(szSVOCommand + 95, "SV2 M%d\r M%d\r", nPosition, nPosition);
	write(m_nsSVO, szSVOCommand, 114);
}

void clsSVO::SetRudder_FeiLion(HELICOPTERRUDDER *pRudder)
{
	HELICOPTERRUDDER rudder_ = *pRudder;

	int nAilPos = (int)(rudder_.aileron*834);
	int nElePos = (int)(rudder_.elevator*834);
	int nThrPos = (int)(rudder_.throttle*834);
	int nRudPos	= (int)(rudder_.rudder*834);

	// Cap the control inputs to protect the servos and motors
	nAilPos = range(nAilPos, -500, 500);
	nElePos = range(nElePos, -500, 500);
	nThrPos = range(nThrPos, -500, 500);
	nRudPos	= range(nRudPos, -500, 500);

//	nAilPos = m_trim_aileron;
	nAilPos = m_trim_aileron + nAilPos;
//	nAilPos = m_trim_aileron + ::sin(0.2*PI*GetTime())*300;

//	nElePos = m_trim_elevator;
	nElePos = m_trim_elevator + nElePos;
//	nElePos = m_trim_elevator + ::sin(0.2*PI*GetTime())*300;
//	nElePos = m_trim_elevator + floor(::sin(0.5*PI*t))*700;

//	nThrPos = m_trim_throttle;
//	nThrPos = m_trim_throttle + nThrPos;
//	nThrPos = m_trim_throttle + ::sin(0.2*PI*t)*300;
//	nThrPos = 2900 + floor(::sin(0.5*PI*t))*400;
	nThrPos = 600;

	nRudPos = m_trim_rudder;
//	nRudPos = m_trim_rudder + nRudPos;
//	nRudPos = m_trim_rudder + ::sin(0.2*PI*t)*300;

	if (m_nCount % 50 == 0 )
	{
		cout<<"Ail: "<<nAilPos<<endl;
		cout<<"Ele: "<<nElePos<<endl;
		cout<<"Thr: "<<nThrPos<<endl;
		cout<<"Rud: "<<nRudPos<<endl;
		cout<<endl;
	}

	// Send to servo controller
    char ail[6], ele[6], thr[6], rud[6];
    ail[0] = ele[0] = thr[0] = rud[0] = 0x80;
    ail[1] = ele[1] = thr[1] = rud[1] = 0x01;
    ail[2] = ele[2] = thr[2] = rud[2] = 0x04;

    // Channel numbers for FeiLion
    ail[3] = 0x00;
    ele[3] = 0x01;
    thr[3] = 0x02;
    rud[3] = 0x03;

	ail[4] = nAilPos / 128;
	ail[5] = nAilPos % 128;
	ele[4] = nElePos / 128;
	ele[5] = nElePos % 128;
	thr[4] = nThrPos / 128;
	thr[5] = nThrPos % 128;
	rud[4] = nRudPos / 128;
	rud[5] = nRudPos % 128;

	write(_im9.m_nsIM9, ail, 6);
	write(_im9.m_nsIM9, ele, 6);
	write(_im9.m_nsIM9, thr, 6);
	write(_im9.m_nsIM9, rud, 6);
}

void clsSVO::SetCamera(double camera)
{
	char szCmd[32];

	//Assert the camera is installed PI/4 down
	double angle = camera + PI/4;
	angle = range(angle, -PI/4, PI/4);

	int nPosition = 15000 + (int)(5000*angle/(PI/4));
	sprintf(szCmd, "SV5 M%d\r", nPosition);
	write(m_nsSVO, szCmd, 11);
}

void clsSVO::WriteCommand()
{
//	tcflush(m_nsSVO, TCIOFLUSH);


	if  (_HELICOPTER == ID_GASSER) {
		write(m_nsSVO, "M?8\rM?9\rM?11\rM?12\rM?13\rM?15\r", 28);
	}

	else if (_HELICOPTER == ID_HELION || _HELICOPTER == ID_SHELION) {
		write(m_nsSVO, "M?9\r", 4);
		write(m_nsSVO, "M?10\r",5);
		write(m_nsSVO, "M?12\r",5);
		write(m_nsSVO, "M?13r",5);
		write(m_nsSVO, "M?14\r",5);
		write(m_nsSVO, "M?8\r", 4);
	}


	m_tRequest = GetTime();
}

// GremLion setting
void clsSVO::WriteCommand1()
{
//	tcflush(m_nsSVO, TCIOFLUSH);

/*	write(m_nsSVO, "M?10\r",5);
	write(m_nsSVO, "M?8\r",4);
	write(m_nsSVO, "M?11\r",5);
	write(m_nsSVO, "M?9\r",4);
	write(m_nsSVO, "M?13\r",5);
	write(m_nsSVO, "M?14\r",5);*/

	write(m_nsSVO, "M?12\r",5);		// elevator
	write(m_nsSVO, "M?10\r",5);		// throttle
	write(m_nsSVO, "M?8\r",4);		// aileron
	write(m_nsSVO, "M?10\r",5);		// auxiliary
	write(m_nsSVO, "M?9\r",4);		// rudder
	write(m_nsSVO, "M?15\r",5);		// toggle
	m_tRequest = GetTime();
}

BOOL clsSVO::GetData(SVORAWDATA *pData)
{
	char szSVO[256];

	SVORAWDATA svo;

	int nRead = read(m_nsSVO, szSVO, 256);
	if (nRead < 0) nRead = 0;
	szSVO[nRead] = '\0';

	int nScanf = sscanf(szSVO, "%hd%hd%hd%hd%hd%hd",
		&svo.throttle, &svo.rudder, &svo.aileron, &svo.elevator, &svo.auxiliary, &svo.sv6);

	if (nScanf != 6) {
		return FALSE;
	}

	*pData = svo;
	m_tRetrieve = m_tRequest + 0.004;

	return TRUE;
}

clsSVO::clsSVO()
{
	pthread_mutexattr_t attr;
	pthread_mutexattr_init(&attr);
	pthread_mutex_init(&m_mtxSVO, &attr);
}

clsSVO::~clsSVO()
{
	pthread_mutex_destroy(&m_mtxSVO);
}

void clsSVO::Translate_HeLion(SVORAWDATA *pSVORAW, SVODATA *pSVO)
{
	pSVO->aileron = (double)(pSVORAW->aileron-15000)/5000;		// normalized to -1 ~ 1
	pSVO->elevator = (double)(pSVORAW->elevator-15000)/5000;	// normalized to -1 ~ 1
	pSVO->auxiliary = (double)(pSVORAW->auxiliary-15000)/5000;	// normalized to -1 ~ 1
	pSVO->rudder = (double)(pSVORAW->rudder-15000)/5000;		// normalized to -1 ~ 1
	pSVO->throttle = (double)(pSVORAW->throttle-10000)/10000;	// normalized to  0 ~ 1

	pSVO->sv6 = (double)(pSVORAW->sv6-15000)/5000;
}

void clsSVO::Translate_GremLion(SVORAWDATA *pSVORAW, SVODATA *pSVO)
{
	pSVO->aileron = (double)(pSVORAW->aileron-15290)/5000;		// normalized to -1 ~ 1
	pSVO->elevator = (double)(pSVORAW->elevator-15516)/5000;	// normalized to -1 ~ 1
	pSVO->auxiliary = (double)(pSVORAW->auxiliary-15000)/5000;	// normalized to -1 ~ 1
	pSVO->rudder = (double)(pSVORAW->rudder-14685)/5000;		// normalized to -1 ~ 1
	pSVO->throttle = (double)(pSVORAW->throttle-15000)/5000;	// normalized to -1 ~ 1
	pSVO->sv6 = (double)(pSVORAW->sv6-15000)/5000;
}

void clsSVO::Translate_GremLion(SVORAWDATA *pSVORAW, SVODATA *pSVO, SVORAWDATA *pSVOTrim, BOOL bManualTrim)
{
	if ( bManualTrim ) {
		pSVO->aileron = (double)(pSVORAW->aileron-pSVOTrim->aileron)/5000;		// normalized to -1 ~ 1
		pSVO->elevator = (double)(pSVORAW->elevator-pSVOTrim->elevator)/5000;		// normalized to -1 ~ 1
		pSVO->auxiliary = (double)(pSVORAW->auxiliary-15000)/5000;	// normalized to -1 ~ 1
		pSVO->rudder = (double)(pSVORAW->rudder-pSVOTrim->rudder)/5000;			// normalized to -1 ~ 1
		pSVO->throttle = (double)(pSVORAW->throttle- 15000 /*pSVOTrim->throttle*/)/5000;		// normalized to -1 ~ 1
		pSVO->sv6 = (double)(pSVORAW->sv6-15000)/5000;
	} else {
		pSVO->aileron = (double)(pSVORAW->aileron-15000)/5000;		// normalized to -1 ~ 1
		pSVO->elevator = (double)(pSVORAW->elevator-15000)/5000;		// normalized to -1 ~ 1
		pSVO->auxiliary = (double)(pSVORAW->auxiliary-15000)/5000;	// normalized to -1 ~ 1
		pSVO->rudder = (double)(pSVORAW->rudder-15000)/5000;			// normalized to -1 ~ 1
		pSVO->throttle = (double)(pSVORAW->throttle-15000)/5000;		// normalized to -1 ~ 1
		pSVO->sv6 = (double)(pSVORAW->sv6-15000)/5000;
	}
}

void clsSVO::SetManualTrimRawData(SVORAWDATA svoraw)
{
	m_svoManualTrimRaw.aileron = svoraw.aileron;
	m_svoManualTrimRaw.elevator = svoraw.elevator;
	m_svoManualTrimRaw.auxiliary = svoraw.auxiliary;
	m_svoManualTrimRaw.rudder = svoraw.rudder;
	m_svoManualTrimRaw.throttle = svoraw.throttle;

}

void clsSVO::SetSvoConfig(char *devPort, short size, short flag, int baudrate)
{
	memcpy(m_svoConfig.devPort, devPort, size);
	m_svoConfig.flag = flag;
	m_svoConfig.baudrate = baudrate;
}